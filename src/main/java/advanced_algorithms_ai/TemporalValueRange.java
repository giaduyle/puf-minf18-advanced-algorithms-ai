package advanced_algorithms_ai;

import java.io.Serializable;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TemporalValueRange<T extends Temporal, V> implements Serializable, Map<T, V> {
  private static final long serialVersionUID = -60320823215648711L;

  private final int range;
  private final TemporalUnit unit;
  private final T start;
  private final T end;

  private BitSet occupations;
  private V[] values;

  @SuppressWarnings("unchecked")
  public TemporalValueRange(final T start, final T end, final TemporalUnit unit) {
    long inputRange = unit.between(start, end);
    if (inputRange < 0 || inputRange > Integer.MAX_VALUE) {
      throw new IllegalArgumentException(
          String.format(
              "Invalid range of %d! Only support a range between [1...%d]",
              inputRange, Integer.MAX_VALUE));
    }

    this.range = (int) inputRange + 1;
    this.start = start;
    this.end = end;
    this.unit = unit;
    this.values = (V[]) new Object[this.range];
  }

  public int getRange() {
    return range;
  }

  public TemporalUnit getUnit() {
    return unit;
  }

  public T getStart() {
    return start;
  }

  public T getEnd() {
    return end;
  }

  public boolean hasValue(final T at) {
    return hasValue(fromStart(at));
  }

  public boolean hasValue(final int index) {
    return occupations != null && occupations.get(index);
  }

  public boolean isValidIndex(final int index) {
    return index >= 0 && index < values.length;
  }

  public boolean isValidTemporal(final T at) {
    return start.equals(at) || end.equals(at) || (fromStart(at) >= 0 && tillEnd(at) >= 0);
  }

  public Set<T> toAllTemporalSet() {
    return toTemporalSet(0, fromStart(end));
  }

  @SuppressWarnings("unchecked")
  public Set<T> toTemporalSet(final int from, final int to) {
    throwIfInvalidIndex(from);
    throwIfInvalidIndex(to);
    return IntStream.range(from, to + 1)
        .mapToObj(index -> getTemporal(index).orElse((T) start.plus(index, unit)))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  @SuppressWarnings("unchecked")
  public Optional<T> getTemporal(final int index) {
    if (!isValidIndex(index)) {
      return Optional.empty();
    }
    return Optional.ofNullable((T) start.plus(index, unit));
  }

  public int getIndex(final T temporal) {
    return fromStart(temporal);
  }

  public boolean put(final int index, final V value) {
    if (!isValidIndex(index)) {
      return false;
    }
    values[index] = value;
    if (occupations == null) {
      occupations = new BitSet(range);
    }
    occupations.set(index);
    return true;
  }

  public boolean update(final T at, final Function<Optional<V>, V> updater) {
    return update(fromStart(at), updater);
  }

  public boolean update(final int index, final Function<Optional<V>, V> updater) {
    return put(index, updater.apply(getOptional(index)));
  }

  public V get(final int index) {
    if (!isValidIndex(index)) {
      return null;
    }
    return values[index];
  }

  public Optional<V> getOptional(final int index) {
    return Optional.ofNullable(get(index));
  }

  public Collection<Optional<V>> valuesOptional() {
    return values().stream().map(Optional::ofNullable).collect(Collectors.toList());
  }

  public Collection<V> get(final T from, final T to) {
    throwIfInvalidTemporal(from);
    throwIfInvalidTemporal(to);
    return Arrays.asList(Arrays.copyOfRange(values, fromStart(from), tillEnd(to)));
  }

  public Collection<V> get(final int from, final int to) {
    throwIfInvalidIndex(from);
    throwIfInvalidIndex(to);
    return Arrays.asList(Arrays.copyOfRange(values, from, to));
  }

  public V remove(final int index) {
    if (!hasValue(index)) {
      return null;
    }
    final V removed = values[index];
    values[index] = null;
    occupations.clear(index);
    return removed;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void clear() {
    this.values = (V[]) new Object[this.range];
    occupations.clear();
  }

  @SuppressWarnings("unchecked")
  @Override
  public Set<T> keySet() {
    return IntStream.range(0, fromStart(end) + 1)
        .filter(this::hasValue)
        .mapToObj(index -> (T) start.plus((long) index, unit))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  @Override
  public Collection<V> values() {
    return Arrays.asList(values);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Set<Entry<T, V>> entrySet() {
    return IntStream.range(0, fromStart(end) + 1)
        .filter(this::hasValue)
        .mapToObj(index -> new TemporalValueEntry<>((T) start.plus((long) index, unit), get(index)))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  @Override
  public int size() {
    return occupations == null ? 0 : occupations.length();
  }

  @Override
  public boolean isEmpty() {
    return occupations == null || occupations.isEmpty();
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean containsKey(final Object key) {
    if (notTemporalType(key)) {
      return false;
    }
    return isValidTemporal((T) key);
  }

  @Override
  public boolean containsValue(final Object value) {
    if (notValueType(value)) {
      return false;
    }
    return values().stream().anyMatch(currentValue -> currentValue.equals(value));
  }

  @SuppressWarnings("unchecked")
  @Override
  public V get(final Object key) {
    if (notTemporalType(key)) {
      return null;
    }
    return get(fromStart((T) key));
  }

  @Override
  public V put(final T at, final V value) {
    if (!put(fromStart(at), value)) {
      return null;
    }
    return value;
  }

  @SuppressWarnings("unchecked")
  @Override
  public V remove(final Object key) {
    if (notTemporalType(key)) {
      return null;
    }
    return remove(fromStart((T) key));
  }

  @Override
  public void putAll(final Map<? extends T, ? extends V> map) {
    map.forEach(this::put);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof TemporalValueRange)) return false;
    TemporalValueRange<?, ?> that = (TemporalValueRange<?, ?>) o;
    return range == that.range
        && Objects.equals(unit, that.unit)
        && Objects.equals(start, that.start)
        && Objects.equals(end, that.end)
        && Objects.equals(occupations, that.occupations)
        && Arrays.equals(values, that.values);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(range, unit, start, end, occupations);
    result = 31 * result + Arrays.hashCode(values);
    return result;
  }

  @Override
  public String toString() {
    return "TemporalValueRange{"
        + "range="
        + range
        + ", unit="
        + unit
        + ", start="
        + start
        + ", end="
        + end
        + ", values="
        + Arrays.toString(values)
        + '}';
  }

  private int fromStart(final Temporal at) {
    return (int) start.until(at, unit);
  }

  private int tillEnd(final Temporal at) {
    return (int) at.until(end, unit);
  }

  private void throwIfInvalidTemporal(final T at) {
    if (!isValidTemporal(at)) {
      throw new IllegalArgumentException(String.format("Invalid temporal %s!", at));
    }
  }

  private void throwIfInvalidIndex(final int index) {
    if (!isValidIndex(index)) {
      throw new IllegalArgumentException(String.format("Invalid index %d!", index));
    }
  }

  private boolean notTemporalType(final Object object) {
    return !object.getClass().isAssignableFrom(start.getClass());
  }

  private boolean notValueType(final Object object) {
    return !object.getClass().isAssignableFrom(values.getClass());
  }

  private static class TemporalValueEntry<T extends Temporal, V> implements Entry<T, V> {
    private final T temporal;
    private V value;

    TemporalValueEntry(final T temporal, final V value) {
      this.temporal = temporal;
      this.value = value;
    }

    @Override
    public T getKey() {
      return temporal;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public V setValue(V value) {
      this.value = value;
      return value;
    }
  }
}
