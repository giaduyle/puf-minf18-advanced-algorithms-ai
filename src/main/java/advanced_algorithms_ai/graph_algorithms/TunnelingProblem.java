package advanced_algorithms_ai.graph_algorithms;

import java.awt.*;
import java.util.List;
import java.util.Optional;

public class TunnelingProblem {
  private static final String PATH = ".";
  private static final String WALKED_PATH = "o";
  private static final String WALL = "#";
  private static final int ID_FACTOR = 8;

  public static void main(final String[] args) {
    // Todo: run the program.
  }

  static String[][] loadMaze(final List<String> lines) {
    final int rows = Integer.parseInt(lines.get(0));
    final int columns = Integer.parseInt(lines.get(1));
    return readMaze(lines.subList(2, lines.size() - 2), rows, columns);
  }

  static int loadSourceNode(final List<String> lines) {
    return Integer.parseInt(lines.get(lines.size() - 2));
  }

  static int loadDestNode(final List<String> lines) {
    return Integer.parseInt(lines.get(lines.size() - 1));
  }

  private static String[][] readMaze(final List<String> lines, final int rows, final int columns) {
    final String[][] output = new String[rows][columns];
    for (int i = 0; i < lines.size(); i++) {
      final String[] chars =
          lines.get(i).chars().mapToObj(c -> String.valueOf((char) c)).toArray(String[]::new);
      output[i] = chars;
    }
    return output;
  }

  static Graph walkMaze(final String[][] maze, final int x, final int y) {
    final Point startPos = atBorder(maze, x, y) ? new Point(x, y) : new Point(0, 0);
    final Point entry =
        findEntrance(maze, new String[maze.length][maze[0].length], startPos)
            .orElseThrow(() -> new IllegalArgumentException("Cannot find entrance to the maze"));
    final Graph graph = new Graph();
    walkMaze(maze, entry.x, entry.y, graph, null, 0);
    return graph;
  }

  private static void walkMaze(
      final String[][] maze,
      final int x,
      final int y,
      final Graph graph,
      Node lastNode,
      int weight) {
    if (outOfBorder(maze, x, y)) return;

    final String current = maze[x][y];
    if (current.equals(PATH)) {
      maze[x][y] = WALKED_PATH;
      weight++;
    }

    final Optional<Node> parseResult = parseNode(maze, x, y);
    if (parseResult.isPresent()) {
      final Node currentNode = parseResult.get();
      graph.addNode(currentNode);
      if (lastNode != null) {
        weight--;
        connect(currentNode, lastNode, weight);
      }
      weight = 1;
      lastNode = currentNode;
    }

    // Go down
    final int nextX = x + 1;
    final Optional<Node> downNode = graph.getNode(calculateId(nextX, y));
    if (isPath(maze, nextX, y)) walkMaze(maze, nextX, y, graph, lastNode, weight);
    else if (downNode.isPresent() && lastNode != null) connect(downNode.get(), lastNode, weight);

    // Go right
    final int nextY = y + 1;
    final Optional<Node> rightNode = graph.getNode(calculateId(x, nextY));
    if (isPath(maze, x, nextY)) walkMaze(maze, x, nextY, graph, lastNode, weight);
    else if (rightNode.isPresent() && lastNode != null) connect(rightNode.get(), lastNode, weight);

    // Go left
    final int prevY = y - 1;
    final Optional<Node> leftNode = graph.getNode(calculateId(x, prevY));
    if (isPath(maze, x, prevY)) walkMaze(maze, x, prevY, graph, lastNode, weight);
    else if (leftNode.isPresent() && lastNode != null) connect(leftNode.get(), lastNode, weight);

    // Go up
    final int prevX = x - 1;
    final Optional<Node> upNode = graph.getNode(calculateId(prevX, y));
    if (isPath(maze, prevX, y)) walkMaze(maze, prevX, y, graph, lastNode, weight);
    else if (upNode.isPresent() && lastNode != null) connect(upNode.get(), lastNode, weight);
  }

  private static void connect(final Node source, final Node target, int weight) {
    if (source.equals(target)) return;
    source.biConnect(target, weight);
  }

  private static Optional<Node> parseNode(final String[][] maze, final int x, final int y) {
    if (isWall(maze, x, y)) return Optional.empty();

    if (atBorder(maze, x, y)) return Optional.of(new Node(calculateId(x, y), x, y));

    int countPaths =
        countIfNotWall(maze, x - 1, y)
            + countIfNotWall(maze, x + 1, y)
            + countIfNotWall(maze, x, y - 1)
            + countIfNotWall(maze, x, y + 1);

    if (countPaths % 2 > 0) return Optional.of(new Node(calculateId(x, y), x, y));
    return Optional.empty();
  }

  private static Optional<Point> findEntrance(
      final String[][] maze, final String[][] walked, final Point atPos) {
    if (!atBorder(maze, atPos.x, atPos.y) || outOfBorder(maze, atPos.x, atPos.y))
      return Optional.empty();

    if (walked[atPos.x][atPos.y] == null && isPath(maze, atPos.x, atPos.y))
      return Optional.of(atPos);

    walked[atPos.x][atPos.y] = WALKED_PATH;

    final Optional<Point> up = findEntrance(maze, walked, new Point(atPos.x - 1, atPos.y));
    if (up.isPresent()) return up;

    final Optional<Point> left = findEntrance(maze, walked, new Point(atPos.x, atPos.y - 1));
    if (left.isPresent()) return left;

    final Optional<Point> right = findEntrance(maze, walked, new Point(atPos.x, atPos.y + 1));
    if (right.isPresent()) return right;

    final Optional<Point> bottom = findEntrance(maze, walked, new Point(atPos.x + 1, atPos.y));
    if (bottom.isPresent()) return bottom;

    return Optional.empty();
  }

  private static boolean atBorder(final Object[][] maze, final int x, final int y) {
    return x == 0 || x + 1 == maze.length || y == 0 || y + 1 == maze[0].length;
  }

  private static boolean outOfBorder(final Object[][] maze, final int x, final int y) {
    return x < 0 || x >= maze.length || y < 0 || y >= maze[x].length;
  }

  private static int countIfNotWall(final String[][] maze, final int x, final int y) {
    return isWall(maze, x, y) ? 0 : 1;
  }

  private static boolean isWall(final String[][] maze, final int x, final int y) {
    return maze[x][y].equals(WALL);
  }

  private static boolean isPath(final String[][] maze, final int x, final int y) {
    if (outOfBorder(maze, x, y)) return false;
    return maze[x][y].equals(PATH);
  }

  private static int calculateId(final int x, final int y) {
    return x * TunnelingProblem.ID_FACTOR + y;
  }

  static void logMaze(final String[][] maze) {
    for (String[] rows : maze) {
      for (String pos : rows) System.out.print(pos);
      System.out.println();
    }
  }
}
