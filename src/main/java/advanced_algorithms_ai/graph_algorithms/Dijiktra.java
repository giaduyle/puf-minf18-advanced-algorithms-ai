package advanced_algorithms_ai.graph_algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

class Dijiktra {
  static void calculateShortestPaths(final Node source) {
    source.setDistance(0);
    Set<Node> settledNodes = new HashSet<>();
    Set<Node> unsettledNodes = new HashSet<>();
    unsettledNodes.add(source);
    while (!unsettledNodes.isEmpty()) {
      Node current = getLowestDistance(unsettledNodes);
      unsettledNodes.remove(current);
      for (Edge edge : current.adjacentEdges()) {
        Node adjacentNode = edge.getDest();
        if (!settledNodes.contains(adjacentNode)) {
          calculateMinimumDistance(adjacentNode, edge.getWeight(), current);
          unsettledNodes.add(adjacentNode);
        }
      }
      settledNodes.add(current);
    }
  }

  private static void calculateMinimumDistance(
      final Node evaluation, final int weight, final Node source) {
    int sourceDistance = source.getDistance();
    if (sourceDistance + weight < evaluation.getDistance()) {
      evaluation.setDistance(sourceDistance + weight);
      LinkedList<Node> shortestPath = new LinkedList<>(source.getShortestPaths());
      shortestPath.add(source);
      evaluation.setShortestPaths(shortestPath);
    }
  }

  private static Node getLowestDistance(final Set<Node> unsettled) {
    Node lowestNode = null;
    int lowestDistance = Integer.MAX_VALUE;
    for (Node node : unsettled) {
      int nodeDistance = node.getDistance();
      if (nodeDistance < lowestDistance) {
        lowestDistance = nodeDistance;
        lowestNode = node;
      }
    }
    return lowestNode;
  }
}
