package advanced_algorithms_ai.graph_algorithms;

import java.util.*;

public class Graph {
  private final LinkedHashMap<Integer, Node> nodes;
  private final LinkedHashMap<Node, Edge> edges;

  Graph() {
    nodes = new LinkedHashMap<>();
    edges = new LinkedHashMap<>();
  }

  Graph(final Node... inputNodes) {
    this();
    Arrays.stream(inputNodes)
        .forEach(
            node -> {
              addNode(node);
              node.adjacentEdges().forEach(this::addEdge);
            });
  }

  Map<Integer, Node> nodes() {
    return nodes;
  }

  Optional<Node> firstNode() {
    return Optional.ofNullable(nodes.values().iterator().next());
  }

  Optional<Node> getNode(final int id) {
    return Optional.ofNullable(nodes.get(id));
  }

  Graph addNode(final Node node) {
    nodes.put(node.getId(), node);
    return this;
  }

  Graph addEdge(final Edge edge) {
    edges.put(edge.getSource(), edge);
    return this;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    nodes
        .values()
        .stream()
        .sorted(Comparator.comparingInt(Node::getId))
        .forEach(
            node -> {
              builder.append(node.toString());
              builder.append(System.lineSeparator());
            });
    return builder.toString();
  }
}
