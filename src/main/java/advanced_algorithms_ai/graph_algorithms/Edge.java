package advanced_algorithms_ai.graph_algorithms;

public class Edge {
  private final Node source;
  private final Node dest;
  private final int weight;

  Edge(final Node source, final Node dest, final int weight) {
    this.source = source;
    this.dest = dest;
    this.weight = weight;
  }

  Edge(final Node source, final Node dest) {
    this(source, dest, 0);
  }

  Node getSource() {
    return source;
  }

  Node getDest() {
    return dest;
  }

  int getWeight() {
    return weight;
  }

  @Override
  public String toString() {
    return "Edge{" + "source=" + source + ", dest=" + dest + ", weight=" + weight + '}';
  }
}
