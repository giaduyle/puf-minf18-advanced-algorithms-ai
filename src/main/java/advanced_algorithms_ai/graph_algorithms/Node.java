package advanced_algorithms_ai.graph_algorithms;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Node extends Point {
  private final int id;
  private int distance = Integer.MAX_VALUE;
  private final Map<Integer, Edge> adjacencies = new LinkedHashMap<>();
  private List<Node> shortestPaths = new LinkedList<>();

  Node(final int x, final int y) {
    super(x, y);
    this.id = x + y;
  }

  Node(final int id, final int x, final int y) {
    super(x, y);
    this.id = id;
  }

  Collection<Edge> adjacentEdges() {
    return adjacencies.values();
  }

  int getId() {
    return id;
  }

  int getDistance() {
    return distance;
  }

  void setShortestPaths(List<Node> shortestPaths) {
    this.shortestPaths = shortestPaths;
  }

  List<Node> getShortestPaths() {
    return shortestPaths;
  }

  void setDistance(final int distance) {
    this.distance = distance;
  }

  Edge connect(final Node other) {
    return connect(other, 0);
  }

  Edge connect(final Node other, final int weight) {
    if (hasAdjacency(other)) {
      return adjacencies.get(other.getId());
    }
    final Edge edge = new Edge(this, other, weight);
    adjacencies.put(other.getId(), edge);
    return edge;
  }

  void biConnect(final Node other) {
    biConnect(other, 0);
  }

  void biConnect(final Node other, final int weight) {
    connect(other, weight);
    other.connect(this, weight);
  }

  boolean hasAdjacency(final Node node) {
    return adjacencies.containsKey(node.getId());
  }

  @Override
  public String toString() {
    return getId()
        + adjacentEdges()
            .stream()
            .sorted(Comparator.comparingInt(o -> o.getDest().getId()))
            .map(edge -> String.format("(%s,%d)", edge.getDest().getId(), edge.getWeight()))
            .collect(Collectors.joining());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    Node node = (Node) o;
    return id == node.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), id);
  }
}
