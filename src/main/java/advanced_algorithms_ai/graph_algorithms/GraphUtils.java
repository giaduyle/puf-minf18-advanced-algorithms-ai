package advanced_algorithms_ai.graph_algorithms;

import java.util.HashSet;
import java.util.Set;

public class GraphUtils {
  static boolean isCyclic(final Graph graph) {
    return isDFSCyclic(
        graph.firstNode().orElseThrow(() -> new IllegalStateException("Graph has no nodes!")),
        null,
        new HashSet<>());
  }

  private static boolean isDFSCyclic(
      final Node current, final Node parent, final Set<Node> visited) {
    visited.add(current);
    for (final Edge edge : current.adjacentEdges()) {
      final Node dest = edge.getDest();
      if (!visited.contains(dest)) {
        if (isDFSCyclic(dest, current, visited)) return true;
      } else if (!dest.equals(parent)) return true;
    }
    return false;
  }
}
