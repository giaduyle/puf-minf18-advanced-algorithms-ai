package advanced_algorithms_ai.tictactoe.strategies;

import advanced_algorithms_ai.tictactoe.Board;
import advanced_algorithms_ai.tictactoe.Move;
import advanced_algorithms_ai.tictactoe.PlayStrategy;
import advanced_algorithms_ai.tictactoe.Value;
import java.util.List;

public class ScoredBoard implements PlayStrategy {
  @Override
  public Move nextMove(final Board board) {
    return weightedMove(board, board.nextPlayer());
  }

  private Move weightedMove(final Board board, final Value player) {
    int maxScore = 0;
    Move maxMove = null;
    final List<Move> legalMoves = board.legalMoves();
    for (final Move move : legalMoves) {
      board.push(move);
      int score = board.score(player, 0);
      move.setScore(score);
      if (score > maxScore) {
        maxScore = score;
        maxMove = move;
      }
      board.pop();
    }
    return maxMove;
  }
}
