package advanced_algorithms_ai.tictactoe.strategies;

import advanced_algorithms_ai.tictactoe.Board;
import advanced_algorithms_ai.tictactoe.Move;
import advanced_algorithms_ai.tictactoe.PlayStrategy;
import java.util.Scanner;

public class HumanPlayer implements PlayStrategy {
  private final Scanner READER = new Scanner(System.in);

  @Override
  public Move nextMove(final Board board) {
    System.out.println("Enter x:");
    final int x = READER.nextInt();

    System.out.println("Enter y:");
    final int y = READER.nextInt();

    final Move playerMove = new Move(x - 1, y - 1, board.nextPlayer(), 0);
    if (board.canMove(playerMove)) return playerMove;

    System.out.println(String.format("Cannot move at: x=%d, y=%d", x, y));
    return nextMove(board);
  }
}
