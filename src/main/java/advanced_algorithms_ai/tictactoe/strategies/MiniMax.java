package advanced_algorithms_ai.tictactoe.strategies;

import advanced_algorithms_ai.tictactoe.Board;
import advanced_algorithms_ai.tictactoe.Move;
import advanced_algorithms_ai.tictactoe.PlayStrategy;
import advanced_algorithms_ai.tictactoe.Value;
import java.util.List;

public class MiniMax implements PlayStrategy {
  private final long maxDepths;

  public MiniMax(final int maxDepths) {
    this.maxDepths = maxDepths;
  }

  public MiniMax() {
    this.maxDepths = Long.MAX_VALUE;
  }

  @Override
  public Move nextMove(final Board board) {
    return miniMax(board, board.nextPlayer(), 0);
  }

  private Move miniMax(final Board board, final Value player, int currentDepth) {
    Value nextPlayer = board.nextPlayer();
    Move bestMove =
        new Move(-1, -1, player, nextPlayer == player ? Integer.MIN_VALUE : Integer.MAX_VALUE);

    if (isLeaf(board, currentDepth))
      return board.lastMove().setScore(board.score(player, currentDepth));

    currentDepth++;
    final List<Move> legalMoves = board.legalMoves();
    for (final Move move : legalMoves) {
      board.push(move);
      final Move result = miniMax(board, player, currentDepth);
      board.pop();

      // maximizing player
      if (nextPlayer == player) {
        if (result.getScore() > bestMove.getScore()) bestMove = move.setScore(result.getScore());
        // minimizing player
      } else {
        if (result.getScore() < bestMove.getScore()) bestMove = move.setScore(result.getScore());
      }
    }
    return bestMove;
  }

  private boolean isLeaf(final Board board, final int currentDepth) {
    return currentDepth == maxDepths || board.isOver();
  }
}
