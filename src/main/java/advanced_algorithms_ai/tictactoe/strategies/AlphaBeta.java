package advanced_algorithms_ai.tictactoe.strategies;

import advanced_algorithms_ai.tictactoe.Board;
import advanced_algorithms_ai.tictactoe.Move;
import advanced_algorithms_ai.tictactoe.PlayStrategy;
import advanced_algorithms_ai.tictactoe.Value;
import java.util.List;

public class AlphaBeta implements PlayStrategy {
  private final long maxDepths;

  public AlphaBeta(final int maxDepths) {
    this.maxDepths = maxDepths;
  }

  public AlphaBeta() {
    this.maxDepths = Long.MAX_VALUE;
  }

  @Override
  public Move nextMove(final Board board) {
    final Value nextPlayer = board.nextPlayer();
    final Move alpha = new Move(-1, -1, nextPlayer, Integer.MIN_VALUE);
    final Move beta = new Move(-1, -1, nextPlayer, Integer.MAX_VALUE);
    return alphaBeta(board, nextPlayer, alpha, beta, 0);
  }

  private Move alphaBeta(
      final Board board, final Value player, Move alpha, Move beta, int currentDepth) {
    if (isLeaf(board, currentDepth))
      return board.lastMove().setScore(board.score(player, currentDepth));

    currentDepth++;
    Move bestMove = null;
    final List<Move> legalMoves = board.legalMoves();
    for (final Move move : legalMoves) {
      board.push(move);
      final Move result = alphaBeta(board, player, alpha, beta, currentDepth);
      board.pop();

      // maximizing player
      if (board.nextPlayer() == player) {
        if (result.getScore() > alpha.getScore()) alpha = move.setScore(result.getScore());
        if (alpha.getScore() >= beta.getScore()) return beta;
        bestMove = alpha;
        // minimizing player
      } else {
        if (result.getScore() < beta.getScore()) beta = move.setScore(result.getScore());
        if (alpha.getScore() >= beta.getScore()) return alpha;
        bestMove = beta;
      }
    }
    return bestMove;
  }

  private boolean isLeaf(final Board board, final int currentDepth) {
    return currentDepth >= maxDepths || board.isOver();
  }
}
