package advanced_algorithms_ai.tictactoe.strategies;

import advanced_algorithms_ai.tictactoe.Board;
import advanced_algorithms_ai.tictactoe.Move;
import advanced_algorithms_ai.tictactoe.PlayStrategy;
import java.util.List;
import java.util.Random;

public class RandomMove implements PlayStrategy {
  @Override
  public Move nextMove(final Board board) {
    //Return a random move from the list of possible moves//
    List<Move> allMoves = board.legalMoves();
    return allMoves.get(new Random().nextInt(allMoves.size()));
  }
}
