package advanced_algorithms_ai.tictactoe;

import advanced_algorithms_ai.tictactoe.strategies.*;

public class starterTictacToe {
  public static void main(String[] args) {
    int size = 9;
    int winning = 6;
    Board board = new Board(Value.X, size, winning, new AlphaBeta(5));
    board.play(new HumanPlayer());
  }
}
