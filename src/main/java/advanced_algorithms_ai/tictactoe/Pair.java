package advanced_algorithms_ai.tictactoe;

public class Pair {
  private int x;
  private int y;
  private Value value;

  Pair(final int x, final int y, final Value value) {
    this.x = x;
    this.y = y;
    this.value = value;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  Value getValue() {
    return value;
  }

  void setValue(Value value) {
    this.value = value;
  }

  Pair duplicate() {
    return new Pair(x, y, value);
  }

  @Override
  public String toString() {
    return String.format("%s(%d,%d)", value, x, y);
  }
}
