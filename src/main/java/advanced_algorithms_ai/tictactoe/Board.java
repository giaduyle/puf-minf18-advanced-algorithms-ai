package advanced_algorithms_ai.tictactoe;

import static advanced_algorithms_ai.tictactoe.Value.E;

import java.io.Closeable;
import java.util.*;

public class Board implements Closeable {
  // Class allowing to play Tic-Tac-Toe. It looks quite long but illustrate the  classical behavior of a game, giving methods you can find in some other games
  // (such as chess for example)//

  private int size;
  private int winning;
  private Pair[][] pairs;
  private int[][] weights;
  private LinkedList<LinkedList<Pair>> alignments;
  private Stack<Move> moveStack;
  private final Value inputPlayer;
  private Value nextPlayer;
  private PlayStrategy inputStrategy;

  Board(final Value firstPlayer, final int size, final PlayStrategy inputStrategy) {
    this(firstPlayer, size, size, inputStrategy);
  }

  Board(
      final Value firstPlayer,
      final int size,
      final int winning,
      final PlayStrategy inputStrategy) {
    if (winning < 2 || winning > size)
      throw new IllegalArgumentException("Invalid winning condition!");

    this.inputStrategy = inputStrategy;
    this.size = size;
    this.winning = winning;
    inputPlayer = firstPlayer;
    nextPlayer = firstPlayer;
    initBoard(size);
    initWeights(size);
    initAlignments(size);
    moveStack = new Stack<>(); // Used to keep track of push/pop moves
  }

  Board(final Value nextPlayer, final String[][] boardState, final PlayStrategy inputStrategy) {
    this(nextPlayer, boardState.length, boardState, inputStrategy);
  }

  Board(
      final Value nextPlayer,
      final int winning,
      final String[][] boardState,
      final PlayStrategy inputStrategy) {
    this(nextPlayer, boardState.length, winning, inputStrategy);
    this.nextPlayer = nextPlayer;
    for (int x = 0; x < boardState.length; x++) {
      for (int y = 0; y < boardState[x].length; y++) {
        putValue(x, y, Value.from(boardState[x][y]));
      }
    }
  }

  private void initBoard(final int size) {
    pairs = new Pair[size][size];
    for (int x = 0; x < pairs.length; x++)
      for (int y = 0; y < pairs[x].length; y++) pairs[x][y] = new Pair(x, y, Value.E);
  }

  private void initWeights(final int size) {
    weights = new int[size][size];
    for (int x = 0; x < weights.length; x++) {
      for (int y = 0; y < weights[x].length; y++) {
        if (x == 0 || x == size - 1 || y == 0 || y == size - 1) {
          weights[x][y] = 2;
          continue;
        }
        int center = (size / 2);
        if (x == center || y == center) {
          weights[x][y] = 4;
          continue;
        }
        weights[x][y] = 1;
      }
    }
  }

  private void initAlignments(final int size) {
    alignments = new LinkedList<>();

    // horizontal && vertical alignments
    for (int x = 0; x < size; x++) {
      LinkedList<Pair> horizontal = new LinkedList<>();
      LinkedList<Pair> vertical = new LinkedList<>();
      for (int y = 0; y < size; y++) {
        horizontal.add(new Pair(x, y, E));
        vertical.add(new Pair(y, x, E));
      }
      alignments.add(horizontal);
      alignments.add(vertical);
    }

    // left - right diagonal alignments
    for (int y = 0; y <= size; y++) {
      if (y < size)
        buildDiagonalAlignment(0, y, -1).ifPresent(diagonal -> alignments.add(diagonal));
      else
        for (int x = 1; x < size; x++)
          buildDiagonalAlignment(x, y - 1, -1).ifPresent(diagonal -> alignments.add(diagonal));
    }

    // right - left diagonal alignments
    for (int y = size; y >= 0; y--) {
      if (y < size)
        buildDiagonalAlignment(0, y, +1).ifPresent(diagonal -> alignments.add(diagonal));
      else
        for (int x = 1; x < size; x++)
          buildDiagonalAlignment(x, 0, 1).ifPresent(diagonal -> alignments.add(diagonal));
    }
  }

  private Optional<LinkedList<Pair>> buildDiagonalAlignment(int x, int y, int yVector) {
    LinkedList<Pair> diagonal = new LinkedList<>();
    if (!outOfBoard(x, y)) diagonal.add(new Pair(x, y, E));

    int nextY = y;
    for (int nextX = x + 1; nextX < size; nextX++) {
      nextY += yVector;
      if (outOfBoard(nextX, nextY)) break;
      diagonal.add(new Pair(nextX, nextY, E));
    }

    if (diagonal.size() >= winning) return Optional.of(diagonal);
    return Optional.empty();
  }

  private Optional<Value> checkAnAlignment() {
    for (LinkedList<Pair> alignment : alignments) {
      final Optional<Value> result = isAlignmentPassed(alignment);
      if (result.isPresent()) return result;
    }
    return Optional.empty();
  }

  private Optional<Value> isAlignmentPassed(final List<Pair> alignment) {
    Value last = null;
    int count = 0;
    for (Pair pair : alignment) {
      final Value value = valueAt(pair);
      if (value == Value.E || (last != null && value != last)) count = 0;

      if (value != Value.E) last = value;

      if (value == last) count++;

      if (count == winning) return Optional.ofNullable(last);
    }
    return Optional.empty();
  }

  public boolean canMove(final Move move) {
    final int x = move.getX();
    final int y = move.getY();
    if (outOfBoard(x, y)) return false;
    return valueAt(x, y) == Value.E;
  }

  private boolean outOfBoard(final int x, final int y) {
    return x < 0 || x >= size || y < 0 || y >= size;
  }

  private Value valueAt(final Pair pair) {
    return valueAt(pair.getX(), pair.getY());
  }

  private Value valueAt(final int x, final int y) {
    return pairs[x][y].getValue();
  }

  private void putValue(final int x, final int y, final Value value) {
    pairs[x][y].setValue(value);
  }

  private boolean atLeastOneEmptyCell() {
    for (int x = 0; x < size; x++)
      for (int y = 0; y < size; y++) if (pairs[x][y].getValue() == Value.E) return true;
    return false;
  }

  public boolean isOver() {
    // Test if the game is over//
    return checkAnAlignment().isPresent() || !atLeastOneEmptyCell();
  }

  Value getResult() {
    //Function to evaluate the victory (or not) as X. Return 1 for victory, 0  for draw and -1 for lose. //
    return checkAnAlignment().orElse(Value.E);
  }

  public Value nextPlayer() {
    return nextPlayer;
  }

  public void push(Move move) {
    // Allows to push a move to be able to unplay it later.//
    // [player, x, y] = move
    Value player = move.getValue();
    int x = move.getX();
    int y = move.getY();
    assert player.equals(nextPlayer);
    moveStack.push(move);
    putValue(x, y, player);
    nextPlayer = opponentOf(nextPlayer);
  }

  static Value opponentOf(final Value player) {
    return player == Value.X ? Value.O : Value.X;
  }

  public void pop() {
    // Pop a move previously played. Allows to put back the pairs in the same state  as before playing.//
    Move move = moveStack.pop();
    Value player = move.getValue();
    int x = move.getX();
    int y = move.getY();

    nextPlayer = player;
    putValue(x, y, Value.E);
  }

  public Move lastMove() {
    return moveStack.peek();
  }

  public List<Move> legalMoves() {
    // An important function : it allows to return all the possible moves for the  current pairs//
    List<Move> moves = new LinkedList<>();
    for (int x = 0; x < size; x++)
      for (int y = 0; y < size; y++)
        if (pairs[x][y].getValue() == Value.E) {
          Move tmp = new Move(x, y, nextPlayer);
          moves.add(tmp);
        }
    return moves;
  }

  public int score(final Value player, final int depth) {
    final Value winner = getResult();
    if (winner == player) return 100 - depth;

    if (winner == opponentOf(player)) return -100 + depth;

    final int heuristic = heuristicScore(player);
    if (nextPlayer == player) return heuristic - depth;
    return -heuristic + depth;
  }

  private int heuristicScore(final Value player) {
    int score = 0;
    for (List<Pair> align : alignments) {
      int playerCount = 0;
      int opponentCount = 0;
      for (Pair pair : align) {
        if (valueAt(pair) == player) playerCount++;
        else opponentCount++;
      }

      int weight = 0;
      if (opponentCount == 0) weight += playerCount * getAdvantage(player, lastMove());

      if (playerCount == 0)
        weight += -(opponentCount * getAdvantage(opponentOf(player), lastMove()));

      score += weight;
    }
    return score;
  }

  private int getAdvantage(final Value player, final Pair pair) {
    int advantage = (nextPlayer == player) ? 2 : 1;
    int weight = valueAt(pair) == E ? weights[pair.getX()][pair.getY()] : 1;
    return advantage * weight;
  }

  void play(final PlayStrategy playStrategy) {
    //Play the Tic-Tac-Toe game randomly.//
    if (moveStack.isEmpty())
      System.out.println(
          String.format("Start playing game: size = %d, win condition = %d", size, winning));

    System.out.println("\r\n\r\n----------");
    System.out.println(this);

    if (isOver()) {
      Value res = getResult();
      if (res == Value.X) System.out.println("Value X won!");
      else if (res == Value.O) System.out.println("Value O won!");
      else System.out.println("Draw");
      return;
    }

    final Move nextMove =
        nextPlayer == inputPlayer ? inputStrategy.nextMove(this) : playStrategy.nextMove(this);
    push(nextMove);
    play(playStrategy);
    pop();
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    for (int x = 0; x < size; x++) {
      if (x == 0) {
        for (int y = 0; y < size; y++) result.append(String.format(" %d ", y + 1));
        result.append("\n\n");
      }
      for (int y = 0; y < size; y++) result.append(pairs[x][y].getValue().toString());
      result.append("\t\t").append(x + 1).append("\n");
    }
    result.append("\nNext player:").append(nextPlayer).append("\n");
    return result.toString();
  }

  @Override
  public void close() {
    this.pairs = null;
    this.alignments.clear();
    this.moveStack.clear();
  }
}
