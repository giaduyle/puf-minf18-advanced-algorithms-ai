package advanced_algorithms_ai.tictactoe;

public enum Value {
  X("X"),
  O("O"),
  E(".");

  private final String value;

  Value(final String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }

  public static Value from(final String input) {
    for (final Value value : Value.values()) {
      if (value.value().equalsIgnoreCase(input)) return value;
    }
    return null;
  }

  @Override
  public String toString() {
    return String.format(" %s ", value);
  }
}
