package advanced_algorithms_ai.tictactoe;

public interface PlayStrategy {
  Move nextMove(final Board board);
}
