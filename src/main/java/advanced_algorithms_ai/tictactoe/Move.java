package advanced_algorithms_ai.tictactoe;

public class Move extends Pair {
  private int score = 0;

  Move(int x, int y, Value value) {
    super(x, y, value);
  }

  public Move(int x, int y, Value value, int score) {
    super(x, y, value);
    this.score = score;
  }

  public int getScore() {
    return score;
  }

  public Move setScore(int score) {
    this.score = score;
    return this;
  }

  @Override
  public String toString() {
    return String.format("%s: %d", super.toString(), score);
  }
}
