package advanced_algorithms_ai;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import org.junit.Assert;
import org.junit.Test;

public class TemporalValueRangeTests {
  @Test
  public void test() {
    // GIVEN
    final ChronoUnit unit = ChronoUnit.MONTHS;
    final LocalDateTime start = LocalDateTime.now();
    final LocalDateTime end = start.plus(12, unit);

    // WHEN
    final TemporalValueRange<LocalDateTime, Long> range =
        new TemporalValueRange<>(start, end, unit);

    // THEN
    Assert.assertTrue(range.isValidTemporal(start));
    Assert.assertTrue(range.isValidTemporal(start.plusSeconds(1)));
    Assert.assertTrue(range.isValidTemporal(start.minusSeconds(1)));
    Assert.assertFalse(range.isValidTemporal(start.minus(1, unit)));

    Assert.assertTrue(range.isValidTemporal(end));
    Assert.assertTrue(range.isValidTemporal(end.minusSeconds(1)));
    Assert.assertTrue(range.isValidTemporal(end.plusSeconds(1)));
    Assert.assertFalse(range.isValidTemporal(end.plus(1, unit)));

    // WHEN
    final Long putValue = range.put(start, 15L);

    // THEN
    Assert.assertTrue(range.hasValue(0));
    Assert.assertFalse(range.hasValue(1));
    Assert.assertTrue(range.hasValue(start));
    Assert.assertFalse(range.hasValue(end));
    Assert.assertNotNull(putValue);
    Assert.assertEquals(15L, (long) range.get(0));
    Assert.assertEquals(15L, (long) range.get(start));
    Assert.assertEquals(15L, (long) range.get(start.plusHours(156)));
    Assert.assertNull(range.get(1));
    Assert.assertNull(range.get(start.plusMonths(1)));
    Assert.assertNull(range.get(start.plusDays(32)));

    // WHEN
    range.update(start.plusDays(15), value -> value.orElse(1L) * 2);
    range.update(start.plusMonths(1), value -> value.orElse(25L) + 1);

    // THEN
    Assert.assertEquals(30L, (long) range.get(start));
    Assert.assertEquals(26L, (long) range.get(start.plusMonths(1)));
  }
}
