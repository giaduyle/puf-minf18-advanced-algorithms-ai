package advanced_algorithms_ai.graph_algorithms;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;

public class TunnelingProblemTests {
  @Test
  public void testShortestPath() throws IOException {
    // GIVEN
    final List<String> lines =
        Files.readAllLines(Paths.get("src", "test", "resources", "maze_input.txt"));
    final int sourceId = TunnelingProblem.loadSourceNode(lines);
    final int destId = TunnelingProblem.loadDestNode(lines);

    // WHEN
    final String[][] maze = TunnelingProblem.loadMaze(lines);

    final Graph graph = TunnelingProblem.walkMaze(maze, 0, 0);
    System.out.println("\nWalked maze:");
    TunnelingProblem.logMaze(maze);

    // THEN
    Assert.assertEquals(
        Files.readAllLines(Paths.get("src", "test", "resources", "graph_expected.txt")),
        Files.readAllLines(toDiagramFile(graph, Paths.get("target", "diagram.txt"))));

    // WHEN
    Dijiktra.calculateShortestPaths(
        graph
            .getNode(sourceId)
            .orElseThrow(() -> new IllegalArgumentException("Not found source node!")));

    // THEN
    Assert.assertEquals(
        Files.readAllLines(Paths.get("src", "test", "resources", "shortest_expected.txt")),
        Files.readAllLines(
            toShortestPathFile(
                graph
                    .getNode(destId)
                    .orElseThrow(() -> new IllegalArgumentException("Not found dest node!")),
                Paths.get("target", "shortest_path.txt"))));
  }

  private Path toDiagramFile(final Graph graph, final Path outputFile) throws IOException {
    cleanOutputFiles(outputFile);
    Files.write(outputFile, graph.toString().getBytes(StandardCharsets.UTF_8));
    return outputFile;
  }

  private Path toShortestPathFile(final Node destNode, final Path outputFile) throws IOException {
    cleanOutputFiles(outputFile);
    final String shortestPath =
        destNode
            .getShortestPaths()
            .stream()
            .map(node -> String.valueOf(node.getId()))
            .collect(Collectors.joining("-", "", String.format("-%d", destNode.getId())));
    final String shortestDistance = String.valueOf(destNode.getDistance());

    final List<String> lines = Arrays.asList(shortestPath, shortestDistance);
    Files.write(outputFile, lines);
    return outputFile;
  }

  private void cleanOutputFiles(final Path outputFile) throws IOException {
    if (!Files.exists(outputFile.getParent())) {
      Files.createDirectory(outputFile.getParent());
    }
    Files.deleteIfExists(outputFile);
    Files.createFile(outputFile);
  }
}
