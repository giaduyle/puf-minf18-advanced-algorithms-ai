package advanced_algorithms_ai.graph_algorithms;

import org.junit.Assert;
import org.junit.Test;

public class GraphTests {
  @Test
  public void testCyclic() {
    // GIVEN
    final Node nodeA = new Node(0, 1);
    final Node nodeB = new Node(0, 2);
    final Node nodeC = new Node(0, 3);
    final Node nodeD = new Node(0, 4);

    nodeA.biConnect(nodeB);
    nodeB.biConnect(nodeC);
    nodeC.biConnect(nodeA);
    nodeC.biConnect(nodeD);

    // THEN
    Assert.assertTrue(GraphUtils.isCyclic(new Graph(nodeA, nodeB, nodeC)));
  }
}
