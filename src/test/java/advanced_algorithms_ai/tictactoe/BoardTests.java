package advanced_algorithms_ai.tictactoe;

import static advanced_algorithms_ai.tictactoe.Value.O;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class BoardTests {
  @Test
  public void test() {
    // GIVEN
    final Value player = O;
    final String[][] boardState =
        new String[][] {
          //    0    1    2
          {".", ".", "."}, // 0
          {".", ".", "."}, // 1
          {".", ".", "."}, // 2
        };
    final Board board = new Board(player, boardState, null);

    // WHEN
    final int center = moveAndScore(board, player, 1, 1);

    final List<Integer> corners = new ArrayList<>(4);
    corners.add(moveAndScore(board, player, 0, 0));
    corners.add(moveAndScore(board, player, 0, 2));
    corners.add(moveAndScore(board, player, 2, 0));
    corners.add(moveAndScore(board, player, 2, 2));

    final List<Integer> mids = new ArrayList<>(4);
    mids.add(moveAndScore(board, player, 0, 1));
    mids.add(moveAndScore(board, player, 2, 1));
    mids.add(moveAndScore(board, player, 1, 0));
    mids.add(moveAndScore(board, player, 1, 2));

    // THEN
    corners.forEach(score -> Assert.assertTrue(score < center));
    mids.forEach(score -> Assert.assertTrue(score < center));
    for (Integer corner : corners) {
      for (Integer mid : mids) {
        Assert.assertTrue(corner > mid);
      }
    }
  }

  @Test
  public void testAlignment() {
    // GIVEN
    final String[][] boardState =
        new String[][] {
          {".", ".", ".", ".", ".", "X"},
          {".", "O", ".", ".", "X", "."},
          {".", ".", "O", "X", ".", "."},
          {".", ".", ".", "O", ".", "."},
          {".", ".", ".", ".", "O", "."},
          {".", ".", ".", ".", ".", "."},
        };
    final Board board = new Board(Value.X, 4, boardState, null);
    board.push(new Move(3, 0, Value.X, 0));

    // WHEN
    final int score = board.score(Value.O, 0);

    // THEN
    Assert.assertEquals(100, score);
  }

  @Test
  public void testInitAlignment() {
    // GIVEN
    final String[][] boardState =
        new String[][] {
          //    0    1    2    3
          {".", ".", ".", "."}, // 0
          {".", ".", "O", "."}, // 1
          {".", "O", ".", "."}, // 2
          {".", ".", ".", "."}, // 3
        };
    final Board board = new Board(Value.X, 2, boardState, null);
    board.push(new Move(3, 0, Value.X, 0));

    // WHEN
    final int score = board.score(Value.O, 0);

    // THEN
    Assert.assertEquals(100, score);
  }

  private int moveAndScore(final Board board, final Value player, final int x, final int y) {
    board.push(new Move(x, y, player, 0));
    final int score = board.score(player, 0);
    board.pop();
    return score;
  }
}
