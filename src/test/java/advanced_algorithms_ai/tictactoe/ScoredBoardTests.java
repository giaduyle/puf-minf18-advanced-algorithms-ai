package advanced_algorithms_ai.tictactoe;

import advanced_algorithms_ai.tictactoe.strategies.RandomMove;
import advanced_algorithms_ai.tictactoe.strategies.ScoredBoard;
import org.junit.Assert;
import org.junit.Test;

public class ScoredBoardTests {
  @Test
  public void test() {
    // The initial (input) player will be played with RandomMove.
    final Value inputPlayer = Value.X;
    Board board = new Board(inputPlayer, 3, new RandomMove());

    System.out.println("Start playing game");
    board.play(new ScoredBoard());

    final Value result = board.getResult();

    Assert.assertNotSame(result, Board.opponentOf(inputPlayer));
  }
}
